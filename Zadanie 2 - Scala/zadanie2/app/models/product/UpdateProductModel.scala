package models.product

import play.api.libs.json.{Json, OFormat}

case class UpdateProductModel(var name: Option[String], var description: Option[String])

object UpdateProductModel {
	implicit val updateProductJson: OFormat[UpdateProductModel] = Json.format[UpdateProductModel]
}