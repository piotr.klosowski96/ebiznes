package controllers

import models.cart.CartModel
import models.product.ProductModel
import play.api.libs.json.{Json, OFormat, __}
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import repositories.Repository

import java.util.UUID
import javax.inject.Inject

class CartsController @Inject()(
		val controllerComponents: ControllerComponents,
		val productsRepository: Repository[ProductModel, UUID],
		val cartRepository: Repository[CartModel, UUID]
	) extends BaseController {

	def createCart(): Action[AnyContent] = Action { implicit request =>
		val newCart: Option[CartModel] =
			request.body.asJson.flatMap(
				Json.fromJson[CartModel](_).asOpt
			)

		newCart match {
			case Some(cart) =>
				val missingProductsIds = checkForUndefinedProducts(cart)
				if (missingProductsIds.isEmpty) {
					val cartToBeAdded = CartModel(
						UUID.randomUUID(),
						newCart.get.name,
						newCart.get.products,
					)
					val addedCart = cartRepository.add(cartToBeAdded)
					Ok(Json.toJson(addedCart))
				} else {
					NotFound(Json.toJson(missingProductsIds))
				}
			case None => BadRequest
		}
	}

//	def getAllCarts: Action[AnyContent] = Action {
//		Ok(Json.toJson(cartRepository.getAll))
//	}
//
//	def getCartById(cartID: UUID): Action[AnyContent] = Action {
//		cartRepository.getById(cartID) match {
//			case Some(cart) => Ok(Json.toJson(cart))
//			case None => NotFound
//		}
//	}
//
//	def updateCart(cartID: UUID): Action[AnyContent] = Action {
//
//	}
//
//	def deleteCart(cartID: UUID): Action[AnyContent] = Action {
//		cartRepository.delete(cartID)
//		NoContent
//	}
//
//	def addProductToCart(cartID: UUID, productId: UUID): Action[AnyContent] = Action {
//		cartRepository.getById(cartID) match {
//			case Some(cart) =>
//
//			case None => NotFound
//		}
//	}
//
//	def getProductsInCart(cartID: UUID): Action[AnyContent] = Action {
//		cartRepository.getById(cartID) match {
//			case Some(_) => Ok
//			case None => NotFound
//		}
//	}
//
//	def updateProductsInCart(): Action[AnyContent] = Action {
//
//	}
//
//	def deleteProductFromCart(cartID: UUID, productID: UUID): Action[AnyContent] = Action {
//		cartRepository.getById(cartID) match {
//			case None => NotFound
//			case Some(cart) =>
//				cart.products.filterNot(p => )
//		}
//	}

	private def checkForUndefinedProducts(cart: CartModel) = cart.products
		.map(id => (id, productsRepository.getById(id)))
		.filter(((_: UUID, product: Option[ProductModel]) => product.isEmpty).tupled)
		.map(((id: UUID, _: Option[ProductModel]) => id).tupled)

}
