package models.product

import play.api.libs.functional.syntax.{toApplicativeOps, toFunctionalBuilderOps}
import play.api.libs.json.Reads._
import play.api.libs.json._

case class NewProductModel(var name: String, var description: String)

object NewProductModel {
	implicit def newProductModelReads: Reads[NewProductModel] = (
		(JsPath \ "name").read[String](minLength[String](1) keepAnd maxLength[String](255)) and
		(JsPath \ "description").read[String](minLength[String](1))
	)(NewProductModel.apply _)


}