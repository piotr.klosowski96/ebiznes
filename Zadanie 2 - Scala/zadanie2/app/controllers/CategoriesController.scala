package controllers

import models.category.{CategoryModel, NewCategoryModel, UpdateCategoryModel}
import play.api.libs.json.{Json, OFormat}
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import repositories.Repository

import java.util.UUID
import javax.inject.Inject

class CategoriesController @Inject()(val controllerComponents: ControllerComponents, val categoryRepository: Repository[CategoryModel, UUID]) extends BaseController {
	def addCategory(): Action[AnyContent] = Action { implicit request =>
		val newCategory: Option[NewCategoryModel] =
			request.body.asJson.flatMap(
				Json.fromJson[NewCategoryModel](_).asOpt
			)

		newCategory match {
			case Some(category) =>
				val newCategory = categoryRepository.add(CategoryModel(
					UUID.randomUUID(),
					category.name,
					category.description
				))
				Created(Json.toJson(newCategory))
			case None => BadRequest
		}
	}

	def getCategories: Action[AnyContent] = Action {
		Ok(Json.toJson(categoryRepository.getAll))
	}

	def getCategoryById(productID: UUID): Action[AnyContent] = Action {
		categoryRepository.getById(productID) match {
			case Some(category) => Ok(Json.toJson(category))
			case None => NotFound
		}
	}

	def updateCategory(productID: UUID): Action[AnyContent] = Action { implicit request =>
		val updateCategory: Option[UpdateCategoryModel] =
			request.body.asJson.flatMap(
				Json.fromJson[UpdateCategoryModel](_).asOpt
			)

		val oldCategory = categoryRepository.getById(productID)
		if (oldCategory.isEmpty) {
			NotFound(Json.toJson(s"product with id ${productID.toString} does not exist"))
		} else {
			val updatedCategory = categoryRepository.update(productID, CategoryModel(
				productID,
				updateCategory.get.name.getOrElse(oldCategory.get.name),
				updateCategory.get.description.getOrElse(oldCategory.get.description)
			))
			updatedCategory match {
				case Some(updatedCategory) => Ok(Json.toJson(updatedCategory))
				case None => InternalServerError
			}
		}
	}

	def deleteCategory(productID: UUID): Action[AnyContent] = Action {
		categoryRepository.delete(productID)
		NoContent
	}
}