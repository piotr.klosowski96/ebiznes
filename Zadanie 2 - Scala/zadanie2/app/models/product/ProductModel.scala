package models.product

import play.api.libs.json.{JsPath, Json, OFormat, Reads}
import repositories.WithID

import java.util.UUID

case class ProductModel(var id: UUID, var name: String, var description: String) extends WithID[UUID]

object ProductModel {
	implicit val productJson: OFormat[ProductModel] = Json.format[ProductModel]
}