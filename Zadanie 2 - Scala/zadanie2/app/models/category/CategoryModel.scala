package models.category

import play.api.libs.json.{Json, OFormat}
import repositories.WithID

import java.util.UUID

case class CategoryModel(var id: UUID, var name: String, var description: String) extends WithID[UUID]

object CategoryModel {
	implicit val categoryJson: OFormat[CategoryModel] = Json.format[CategoryModel]
}