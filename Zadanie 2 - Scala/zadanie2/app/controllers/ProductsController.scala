package controllers

import models.product.{NewProductModel, ProductModel, UpdateProductModel}
import play.api.libs.json.{JsValue, Json, OFormat}
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}
import repositories.Repository

import java.util.UUID
import javax.inject.Inject

class ProductsController @Inject()(val controllerComponents: ControllerComponents, val productsRepository: Repository[ProductModel, UUID]) extends BaseController {
  def addProduct(): Action[JsValue] = Action(parse.json) { implicit request =>
    request.body.validate[NewProductModel].fold(
      invalid = {
        fieldErrors => BadRequest(Json.toJson(fieldErrors.toString()))
      },
      valid = {
        product => {
          val newProduct = productsRepository.add(ProductModel(
            UUID.randomUUID(),
            product.name,
            product.description
          ))
          Created(Json.toJson(newProduct))
        }
      }
    )
  }

  def getProducts: Action[AnyContent] = Action {
    Ok(Json.toJson(productsRepository.getAll))
  }

  def getProductById(productID: UUID): Action[AnyContent] = Action {
    productsRepository.getById(productID) match {
      case Some(product) => Ok(Json.toJson(product))
      case None => NotFound
    }
  }

  def updateProduct(productID: UUID): Action[AnyContent] = Action { implicit request =>
    val updateProduct: Option[UpdateProductModel] =
      request.body.asJson.flatMap(
        Json.fromJson[UpdateProductModel](_).asOpt
      )

    val oldProduct = productsRepository.getById(productID)
    if (oldProduct.isEmpty) {
      NotFound(Json.toJson(s"product with id ${productID.toString} does not exist"))
    } else {
      val updatedProduct = productsRepository.update(productID, new ProductModel(
        productID,
        updateProduct.get.name.getOrElse(oldProduct.get.name),
        updateProduct.get.description.getOrElse(oldProduct.get.description)
      ))
      updatedProduct match {
        case Some(updatedProduct) => Ok(Json.toJson(updatedProduct))
        case None => InternalServerError
      }
    }
  }

  def deleteProduct(productID: UUID): Action[AnyContent] = Action {
    productsRepository.delete(productID)
    NoContent
  }
}
