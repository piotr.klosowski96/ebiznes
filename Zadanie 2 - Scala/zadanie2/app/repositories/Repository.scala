package repositories

import scala.collection.mutable

trait WithID[IDType] {
	var id: IDType
}

class Repository[ResourceType <: WithID[IDType], IDType] {
	private val list = new mutable.ListBuffer[ResourceType]()

	def add(resource: ResourceType): Option[ResourceType] = {
		list.find(_.id == resource.id) match {
		case Some(_) => None
		case None =>
			list.addOne(resource)
			Some(resource)
		}
	}

	def getAll: Iterable[ResourceType] = {
		list.toList
	}

	def getById(id: IDType): Option[ResourceType] = {
		list.find(_.id == id)
	}

	def update(id: IDType, resource: ResourceType): Option[ResourceType] = {
		list.zipWithIndex.find(((resource: ResourceType, _: Int) => resource.id == id).tupled) match {
			case Some((_, index)) =>
				list.update(index, resource)
				Option(resource)
			case None => None
		}
	}

	def delete(id: IDType): Boolean = {
		list.zipWithIndex.find(((resource: ResourceType, _: Int) => resource.id == id).tupled) match {
			case Some((_, index)) =>
				list.remove(index)
				true
			case None => false
		}
	}
}
