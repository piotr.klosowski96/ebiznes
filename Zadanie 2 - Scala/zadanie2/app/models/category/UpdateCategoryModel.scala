package models.category

import play.api.libs.json.{Json, OFormat}

case class UpdateCategoryModel(var name: Option[String], var description: Option[String])

object UpdateCategoryModel {
	implicit val updateCategoryJson: OFormat[UpdateCategoryModel] = Json.format[UpdateCategoryModel]
}