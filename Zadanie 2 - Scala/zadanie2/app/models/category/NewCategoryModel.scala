package models.category

import play.api.libs.json.{Json, OFormat}

case class NewCategoryModel(var name: String, var description: String)

object NewCategoryModel {
	implicit val newCategoryJson: OFormat[NewCategoryModel] = Json.format[NewCategoryModel]
}